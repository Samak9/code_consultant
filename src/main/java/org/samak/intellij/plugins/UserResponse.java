package org.samak.intellij.plugins;

import java.awt.Component;
import java.awt.Font;

public class UserResponse implements Response {
	private String response;

	public UserResponse(String response) {
		this.response = response;
	}

	public String getResponse() {
		return response;
	}

	public String getName() {
		return "You";
	}

	public void style(Component component) {
		component.setFont(component.getFont().deriveFont(Font.ITALIC));
	}

}
