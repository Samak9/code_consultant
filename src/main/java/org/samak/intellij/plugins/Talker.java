package org.samak.intellij.plugins;

public interface Talker {

	String processInput(String input);
}
