package org.samak.intellij.plugins;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Panel extends JPanel {

	private final JPanel mainPanel = new JPanel();
	private final JPanel outputPanel = new JPanel();

	private final JPanel inputPanel = new JPanel();
	private final JTextField input = new JTextField();
	private final JButton sendButton = new JButton("Send");

	private Talker talker;
	private final Component bottomComonent = createDefaultBottomComponent();

	public Panel() {
		super(new BorderLayout());
		initComponents();
		initTalker();
	}

	private Component createDefaultBottomComponent() {
		TalkerResponse response = new TalkerResponse("");

		JTextArea component = new JTextArea();
		response.style(component);
		component.setLineWrap(true);
		component.setWrapStyleWord(true);
		component.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		component.setText(response.getResponse());
		component.setRows(Integer.MAX_VALUE);
		component.setEditable(false);
		component.setBackground(Color.WHITE);
		return component;
	}

	private void initComponents() {
		initMain();
		initOutPutPanel();
		initInput();
	}

	private void initMain() {
		mainPanel.setLayout(new BorderLayout());
		add(mainPanel);
	}

	private void initOutPutPanel() {
		outputPanel.setLayout(new BoxLayout(outputPanel, BoxLayout.Y_AXIS));
		mainPanel.add(outputPanel, BorderLayout.CENTER);

		addResponse(new TalkerResponse("Hello, I'm your Code Consulatant."));
		addResponse(new TalkerResponse("Ever notice how just talking through an issue with a co-worker is enough to help you solve a problem, even if they don't say a word?"));
		addResponse(new TalkerResponse("Let me help you with your problems so they can keep working."));
	}

	private void initInput() {
		inputPanel.setLayout(new BoxLayout(inputPanel, BoxLayout.X_AXIS));

		mainPanel.add(inputPanel, BorderLayout.SOUTH);

		// TextField
		input.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					sendButton.doClick();
				}
			}
		});
		inputPanel.add(input);

		// SendButton
		sendButton.setAction(new AbstractAction("Send") {
			@Override
			public void actionPerformed(ActionEvent e) {
				String enteredText = input.getText();
				input.setText("");
				addResponse(new UserResponse(enteredText));
				String response = talker.processInput(enteredText);
				addResponse(new org.samak.intellij.plugins.TalkerResponse(response));
				outputPanel.revalidate();
			}
		});
		inputPanel.add(sendButton);

	}

	private void initTalker() {
		try {
			talker = new ElizaTalker();
		} catch (Exception e) {
			outputPanel.add(createResponseComponent("Error with consultant: " + e.getMessage()));
			e.printStackTrace();
		}
	}

	private void addResponse(final Response response) {
		outputPanel.remove(bottomComonent);

		outputPanel.add(createResponseComponent(response));

		outputPanel.add(bottomComonent, outputPanel.getComponentCount());
	}

	private Component createResponseComponent(Object value) {
		return new JLabel(value.toString());
	}

	private Component createResponseComponent(Response response) {
		JTextArea component = new JTextArea();
		response.style(component);
		component.setLineWrap(true);
		component.setWrapStyleWord(true);
		component.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		component.setText(response.getResponse());
		component.setRows(component.getLineCount());
		component.setEditable(false);
		component.setBackground(Color.WHITE);

		return component;
	}
}
